


// keyup - is an event when user lets go of a key. Keyup is best used in input elements that require key inputs.

let inputFirstName = document.querySelector("#txt-first-name");
let fullNameDisplay = document.querySelector("#full-name-display");
let inputLastName = document.querySelector("txt-last-name");
console.log(inputFirstName);
console.log(inputFirstName.value);


// select the h3 and save it in a variable


 // syntax: element.addEventListener(<event>,function>)

// you can create a name function that will be run by event listeners
 const showName = () => {
 	console.log(inputFirstName.value);
 	console.log(inputLastName.value);

 	// fullNameDisplay.innerHTML = inputFirstName.value + " " + inputLastName.value
 	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
 }

inputFirstName.addEventListener('keyup',showName);

// you can actually create multiple event listeners that run the same function:
inputLastName.addEventListener('keyup',showName);





