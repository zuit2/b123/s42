let heading = document.querySelector("#heading-display");
let inputFirstName = document.querySelector("#txt-first-name");
let inputLastName = document.querySelector("#txt-last-name");
let submitButton = document.querySelector("#submit-btn");

// console.log(inputFirstName);
console.log(inputFirstName.value);

const changeHeading = () => {

	heading.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;

}

const submitForm = () => {

	console.log("Event: submitButton clicked");
	console.log(inputFirstName.value);
	console.log(inputLastName.value);

	if (inputFirstName.value === "" || inputLastName.value === "") {
		alert('Please input firstname and lastname');
	} else {
		alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}`);
		inputFirstName.value = "";
		inputLastName.value = "";
		heading.innerHTML = "";

	}

}

inputFirstName.addEventListener('keyup',changeHeading);
inputLastName.addEventListener('keyup',changeHeading);
submitButton.addEventListener('click',submitForm);